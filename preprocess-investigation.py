from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import accuracy_score, f1_score
from nltk.stem import WordNetLemmatizer, PorterStemmer,SnowballStemmer
from nltk import word_tokenize
import timeit
import sys

class LemmaTokenizer(object):
    def __init__(self):
      self.wnl = WordNetLemmatizer()

    def __call__(self, doc):
        return [self.wnl.lemmatize(t) for t in word_tokenize(doc)]

class PorterTokenizer(object):
    def __init__(self):
        self.stemmer = PorterStemmer()

    def __call__(self, doc):
        return [self.stemmer.stem(t) for t in word_tokenize(doc)]

class SnowballTokenizer(object):
    def __init__(self):
        self.stemmer = SnowballStemmer('english')

    def __call__(self, doc):
        return [self.stemmer.stem(t) for t in word_tokenize(doc)]


output_separator = "======================================="
news_train = fetch_20newsgroups(subset='train')
news_test = fetch_20newsgroups(subset='test')

def perform_classification(vectorizer):
    start = timeit.default_timer()
    train = vectorizer.fit_transform(news_train.data)
    test = vectorizer.transform(news_test.data)
    stop = timeit.default_timer()
    print("Data vectorization time: %0.3f" % (stop - start))

    classifier = MultinomialNB()
    start = timeit.default_timer()
    classifier.fit(train, news_train.target)
    stop = timeit.default_timer()
    print("Model fit time: %0.3f" % (stop - start))

    res = classifier.predict(test)
    score = accuracy_score(news_test.target, res)
    f1 = f1_score(news_test.target, res, average='macro')
    print("F-1 Score:   %0.3f | Accuracy:   %0.3f" % (f1, score))


def main():
    # tfidf_vectorizer = TfidfVectorizer()
    vectorizers = [
        ("No preprocessing",
         CountVectorizer(lowercase=False)),
        ("Lowercase only",
         CountVectorizer()),
        ("Lowercase & stop words",
         CountVectorizer(stop_words='english')),
        ("Lowercase & stop words & Snowball stemmer",
         CountVectorizer(stop_words='english', tokenizer=SnowballTokenizer())),
        ("Lowercase & stop words & Porter stemmer",
         CountVectorizer(stop_words='english', tokenizer=PorterTokenizer())),
        ("Lowercase & stop words & Word Net lemmatizer",
         CountVectorizer(stop_words='english', tokenizer=LemmaTokenizer()))
    ]

    print(output_separator)
    for v in vectorizers:
        print("[%s]\n" % v[0])
        perform_classification(v[1])
        print(output_separator)


if __name__ == "__main__":
    sys.exit(main())
